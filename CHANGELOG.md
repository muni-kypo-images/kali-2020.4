# Changelog


## [qemu-0.1.0], [vbox-0.1.0] - 2021-05-03
### Added
- First version
### Changed
- All users now have ZSH as default shell
- Default user is `kali` instead of `root`

[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali-2020.4/-/tree/qemu-0.1.0
[vbox-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali-2020.4/-/tree/vbox-0.1.0
